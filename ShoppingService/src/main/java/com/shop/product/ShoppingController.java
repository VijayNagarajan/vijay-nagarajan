package com.shop.product;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@RequestMapping("/product/*")
public class ShoppingController {
	
	private ProductService productService;
	@Autowired
	public ShoppingController(ProductService productService) {
		this.productService = productService;
	}

	List<Product> products = new ArrayList<Product>();
	
    @RequestMapping(value = "search", method = RequestMethod.GET)
    public @ResponseBody Product getBestProduct(@RequestParam String name) {
//    	productService.getBestProduct(name);
//    	Product bestProduct = new Product();
//    	if(name.equals("ipad")){
//        	bestProduct.setBestPrice(122.20);
//        	bestProduct.setCurrency("CAD");
//        	bestProduct.setLocation("Walmart");
//        	bestProduct.setProductName("iPad Mini");
//            return bestProduct;
//    	}
//    	else{
//        	bestProduct.setBestPrice(100.20);
//        	bestProduct.setCurrency("CAD");
//        	bestProduct.setLocation("BestBuy");
//        	bestProduct.setProductName("SamSung");
//    	}
    	return productService.getBestProduct(name);
    }
    
    
}
