package com.shop.product;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.web.client.RestTemplate;

public class ProductServiceImpl implements ProductService {
	
    private List<Product> getAllProductsFromWalmart(String productName) {
        RestTemplate restTemplate = new RestTemplate();
        Product WalmartProduct = null;
        JSONParser parser = null;
        JSONArray jsonArray = null;
        JSONObject productAsJSON = null;
        String json = null;
        List<Product> WalMartProductsList = new ArrayList<Product>();
        StringBuffer WalMartURL = new StringBuffer();
        WalMartURL.append(ProductConstants.WALMART_URI);
        WalMartURL.append("?");
        WalMartURL.append("apiKey=");
        WalMartURL.append(ProductConstants.WALMART_APIKEY);
        WalMartURL.append("&");
        WalMartURL.append("query=");
        WalMartURL.append(productName);
        

    	System.out.println("AbsoluteURL" + WalMartURL);
    	try{
            json = restTemplate.getForObject(WalMartURL.toString(), String.class);
            System.out.println("JSON String"  +  json);
            parser = new JSONParser();
            Object obj = parser.parse(json);
            productAsJSON = (JSONObject) obj;
            jsonArray = (JSONArray)productAsJSON.get("items");
//            jsonArray =  (JSONArray)parser.parse(jsonArray.toJSONString()) ;
            System.out.println("product [p]'"
            		+ "count"  +  jsonArray.size());
            for(int count = 0 ; count < jsonArray.size() ; count++ ){
            	productAsJSON = (JSONObject) jsonArray.get(count);
            	WalmartProduct = new Product();
            	WalmartProduct.setProductName(productAsJSON.get("name").toString());
            	WalmartProduct.setBestPrice(Double.valueOf(productAsJSON.get("salePrice").toString()));
            	WalmartProduct.setCurrency("CAD");
            	WalmartProduct.setLocation("WalMart");
            	WalmartProduct.setUniqueProductCode(productAsJSON.get("upc").toString());
            	WalMartProductsList.add(WalmartProduct);
              System.out.println("Product Name" + productAsJSON.get("name"));
              System.out.println("Product Price" + productAsJSON.get("salePrice"));
              System.out.println("Product code" + productAsJSON.get("upc"));
            }
    	}catch(Exception exception){
    		System.out.println("Exception" + exception);
    	}
        return WalMartProductsList;
    }
    
    private List<Product> getAllProductsFromBestBuy(String productName) {
        RestTemplate restTemplate = new RestTemplate();
        Product BestBuyProduct = null;
        JSONParser parser = null;
        JSONArray jsonArray = null;
        JSONObject productAsJSON = null;
        String json = null;
        List<Product> BestBuyProductsList = new ArrayList<Product>();
        StringBuffer BestBuyURL = new StringBuffer();
        BestBuyURL.append(ProductConstants.BESTBUY_URI);
        BestBuyURL.append("(search=");
        BestBuyURL.append(productName);
        BestBuyURL.append(")");
        BestBuyURL.append("?");
        BestBuyURL.append("apiKey=");
        BestBuyURL.append(ProductConstants.BESTBUY_APIKEY);
        BestBuyURL.append("&");
        BestBuyURL.append("format=json");

        
        //we can't get List<Employee> because JSON convertor doesn't know the type of
        //object in the list and hence convert it to default JSON object type LinkedHashMap
    	System.out.println("AbsoluteURL" + BestBuyURL);
    	try{
            json = restTemplate.getForObject(BestBuyURL.toString(), String.class);
            System.out.println("JSON String"  +  json);
            parser = new JSONParser();
            Object obj = parser.parse(json);
            productAsJSON = (JSONObject) obj;
            jsonArray = (JSONArray)productAsJSON.get("products");
//            jsonArray =  (JSONArray)parser.parse(jsonArray.toJSONString()) ;
            System.out.println("product [p]'"
            		+ "count"  +  jsonArray.size());
            for(int count = 0 ; count < jsonArray.size() ; count++ ){
            	productAsJSON = (JSONObject) jsonArray.get(count);
            	BestBuyProduct = new Product();
            	BestBuyProduct.setProductName(productAsJSON.get("name").toString());
            	BestBuyProduct.setBestPrice(Double.valueOf(productAsJSON.get("salePrice").toString()));
            	BestBuyProduct.setCurrency("CAD");
            	BestBuyProduct.setLocation("BestBuy");
            	BestBuyProduct.setUniqueProductCode(productAsJSON.get("productId").toString());
            	BestBuyProductsList.add(BestBuyProduct);
              System.out.println("Product Name" + productAsJSON.get("name"));
              System.out.println("Product Price" + productAsJSON.get("salePrice"));
              System.out.println("Product code" + productAsJSON.get("upc"));
            }
    	}catch(Exception exception){
    		System.out.println("Exception" + exception);
    	}
        return BestBuyProductsList;
    }

	@Override
	public Product getBestProduct(String productName) {
		List<Product> productsList = new ArrayList<Product>();
		productsList.addAll(getAllProductsFromWalmart(productName));
		productsList.addAll(getAllProductsFromBestBuy(productName));
		Collections.sort(productsList);
		// TODO Auto-generated method stub
		return productsList.get(0);
	}



}
