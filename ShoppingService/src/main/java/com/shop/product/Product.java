package com.shop.product;

import java.io.Serializable;

public class Product implements Comparable<Product>,Serializable{
	private String productName;
	private String uniqueProductCode;
	private double bestPrice;
	private String currency;
	private String location;
	
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public double getBestPrice() {
		return bestPrice;
	}
	public void setBestPrice(double bestPrice) {
		this.bestPrice = bestPrice;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getUniqueProductCode() {
		return uniqueProductCode;
	}
	public void setUniqueProductCode(String uniqueProductCode) {
		this.uniqueProductCode = uniqueProductCode;
	}
	
	@Override
	public int compareTo(Product compareProduct) {
		// TODO Auto-generated method stub
		return   (int) (this.bestPrice - compareProduct.bestPrice);
		
	}

	
}
